from python_ui.hubUI import Ui_MainWindow
from PySide2.QtWidgets import QMainWindow, QMessageBox
from AsyncTasks import AsyncTasks
import os
import PySide2


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):  # конструктор
        super().__init__()  # родительский конструктор
        self.setupUi(self)  # создание окна
       
        self.asyncTasks = AsyncTasks(self)  # создание объекта для работы с асинхроннстью

        self.pushButtonStart.clicked.connect(
            self.startModules)  # при нажатии запуск
        # притнажатии стоп остановка, кстати внизу тут можно найти под тонной комментов эту функцию
        self.pushButtonStop.clicked.connect(self.stopModules)
        # по умолчанию кнопка выключена остановки
        self.pushButtonStop.setEnabled(False)

        # если модули еще работают, то нельзя нажать кнопку, так как там имитится freeze
        self.asyncTasks.freeze.connect(
            lambda: self.pushButtonStart.setEnabled(False))
        self.asyncTasks.freeze.connect(
            lambda: self.pushButtonStop.setEnabled(True))  # аналогично

        # если завершилось то даем снова нажать кнопку
        self.asyncTasks.finished.connect(
            lambda: self.pushButtonStart.setEnabled(True))
        self.asyncTasks.finished.connect(
            lambda: self.pushButtonStop.setEnabled(False))  # same

        self.nameOfModule = {}  # создание словаря

        path = os.path.abspath(__file__) # текущий путь файлика
        try: # попытка открыть файл
            f = open(path[:path.rfind('/')] + '/modules.txt', 'r') # в режиме чтения открывает файл, где информация по модулям
            for line in f: # построчкам
                line = line.split() # разбитие на лексемы
                self.nameOfModule[line[0]] = ' '.join(line[1:]) # имя словарь названий модулей (тип внутри программы и то, которое видит пользователь)
            f.close() # закрытие файла
        except FileNotFoundError: # если файл не удалось открыть
            for checkBoxName in self.checkBoxes: # для всех чекбоксов
                self.nameOfModule[checkBoxName] = checkBoxName # имя модуля соотвествует имени чекбокса

        for module in self.nameOfModule: # модуль соответствует именам модулей(в плане именам в словаре)
            self.checkBoxes[module].setText(self.nameOfModule[module]) # непосредственная установка названия в интерфейсе

    # вывод диалогового окна с просьбой выбрать хоть один модуль
    def showWarningMessage(self, title, text):
        msgBox = QMessageBox(self)
        msgBox.setWindowTitle(title)
        msgBox.setText(text)
        msgBox.setIcon(QMessageBox.Warning)
        msgBox.setStandardButtons(QMessageBox.Ok)
        msgBox.setDefaultButton(QMessageBox.Ok)
        msgBox.exec_()

    def closeEvent(self, event):  # при закрытии окна нужно закрывать все таски
        self.asyncTasks.stopAllTasks()  # остановить все задачи
        event.accept()
        
    def startModules(self):  # начало запуска модулей
        modulesDict = dict()  # создание словаря
        
        for checkBoxName in self.checkBoxes:
            modulesDict[checkBoxName] = self.checkBoxes[checkBoxName].isChecked() # проверка выбранны ли они и соотвественно сохранение выбранных

        if not tuple(modulesDict.values()).count(True):  # если ни один не выбрали
            # вот тут вызывается диалоговое окно и передаются параметры
            self.showWarningMessage(
                title="Ни один модуль не выбран", text="Выберите хоть один модуль")
            return  # выходим из данного метода

        mode = '-t' if self.radioButtonTest.isChecked() else '-a'  # сохранение режима работы

        # запускаем выбранные модули
        self.asyncTasks.runModules(modulesDict, mode)

        for module in modulesDict:  # для всех модулей
            if not modulesDict[module]:  # если модуля нету, то пропускаем
                continue

            self.makeTrafficLights(self.asyncTasks.getWorker(module), self.checkBoxes[module], self.nameOfModule[module]) # передача в метод по изменению цветов воркера, чебоксов, имени модулей

    def stopModules(self):  # а вот и она!
        self.pushButtonStop.setEnabled(False) # если остановил, то зачем останавливать -- блокируем нажатие кнопки
        self.asyncTasks.stopAllTasks()  # выполнить метод с остановой всех тасков

    def makeTrafficLights(self, worker, checkbox, nameOfModule):  # светофор
        if worker == None: # если воркера нет то ничего не выполняется
            return

        worker.connected.connect(lambda: checkbox.setText(
            '{0} --- Подключен'.format(nameOfModule)))  # если подключился то меняем чебокс
        worker.connected.connect(lambda: checkbox.setStyleSheet(
            "color: rgb(200, 200, 0)"))  # и его цвет на желто-оранжевый

        worker.working.connect(lambda: checkbox.setText(
            '{0} --- Работает'.format(nameOfModule)))  # если работает
        worker.working.connect(lambda: checkbox.setStyleSheet(
            "color: rgb(0, 200, 70)"))  # зеленый цвет

        self.asyncTasks.stopping.connect(lambda: checkbox.setText(
            '{0} --- Останавливается'.format(nameOfModule)))  # в процессе остановки
        self.asyncTasks.stopping.connect(
            lambda: checkbox.setStyleSheet("color: rgb(150, 150, 0)")) # темно-желто-оранжевый

        # если закончил работу и сигнал из воркера, то возвращаем цвета
        worker.finished.connect(
            lambda: checkbox.setText('{0}'.format(nameOfModule))) # название модуля
        worker.finished.connect(
            lambda: checkbox.setStyleSheet("color: rgb(0, 0, 0)"))

        worker.failed.connect(lambda: checkbox.setText(
            '{0} --- Ошибка'.format(nameOfModule)))  # если неудачная попытка
        worker.failed.connect(
            lambda: checkbox.setStyleSheet("color: rgb(136, 0, 0)")) #  то цвет красный

        # если поступил сигнал об окончании работы, то возвращаем цвета
        self.asyncTasks.finished.connect(
            lambda: checkbox.setText('{0}'.format(nameOfModule)))
        self.asyncTasks.finished.connect(
            lambda: checkbox.setStyleSheet("color: rgb(0, 0, 0)"))
