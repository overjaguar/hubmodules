from PySide2.QtCore import QObject, QThread, Signal
from pathlib import Path
import os
import time
from subprocess import Popen, PIPE
from abc import abstractmethod
import subprocess

class Worker(QObject): #класс с родителем qobject
    connected = Signal() #сигнал о подключении
    working = Signal() #сигнал состояния в работе
    finished = Signal() #для сигнализации окочания работы
    failed = Signal() #сигнал смерти модуля
    message = Signal(str) #для передачи сообщений
    path = str() #путь

    def __init__(self, nameOfModule, mode, pathToModule, table): #конструктор с имением модуля, режимом работы, путем до модуля, и таблице сохранения
        super().__init__() #выполение конструктора родителя
        self.nameOfModule = nameOfModule # имя модуля
        self.pathToModule = pathToModule.resolve() # путь до модуля
        self.table = table # таблица в БД
        self.mode = mode # режим работы
        self.pathToFile = None #по умолчанию пути нету
        self.isWorking = False #по умолчанию не работает типа
        self.userWannaStop = False #юзер по умолчанию не хочет останавливать

    def stop(self): # метод остановки модуля
        if not self.isWorking: # если не работает
            self.finished.emit() # то имит о том, что завершился
            return #если не работает, то выходим
        
        self.userWannaStop = True #пользователь хочет остановить            

        if self.process.poll() == None: # если процесс работает
            if self.mode == '-t': #если была калибровка
                self.process.kill() #то убиваем
            else:
                self.process.communicate(b'\n') #иначе нормального завершаем работу
                self.dbSaving() # выполнение сохранения в бд
        try:         
            self.finished.emit() #имитим завершение работы
        except RuntimeError: # если ошибка то возврат
            return
        
    def run(self): # запуск
        while not self.userWannaStop: #пока пользователь не захочет остановить
            self.process = Popen([self.pathToModule, self.mode], stdin=PIPE, stdout=PIPE, stderr=PIPE) #создание сабпроцесса
            self.path = ''

            self.isWorking = True #флаг работы
            while True: #бесконечный цикл
                line = self.process.stdout.readline()#прочитать строку
                
                if not line: #читаем пока что-то есть, а если нету ничего то выходим из цикла
                    break
                else:
                    line = str(line.rstrip(), 'utf-8') #читаем строку в кодировке ютф8 и убираем ненужные пробелы вконце

                if line.startswith("Results are saved in"):#если встречаем строку, которая начинается с этого
                    self.path = line[len("Results are saved in file: "):] #то сохранение пути сохранения

                    if not self.userWannaStop: #если пользователь не хочет останавливать (только если пришел путь)
                        self.working.emit() #то отсылаем сигналтого, что работает
                        
                if line == "Connected" and not self.userWannaStop and self.mode == '-a': #если встретил строку коннектед
                    self.connected.emit() #имит состояния подключен

                self.message.emit(line)#имитим полученную строку
            
            self.isWorking = False #не работает

            if self.mode == '-t': #если режим работы калибровки то брейкаем
                break

            if self.process.poll() and not self.userWannaStop: #если произошла ошибки
                self.failed.emit() #подача сигнала фейла

            if not self.userWannaStop: # если преждевременно завершился, то происходит запись в бд
                self.dbSaving()

    def dbSaving(self):#запись в бд
        if self.path.strip() == '': # если ничего нету
            return # то возврат

        subprocess.run('cat {1} | pv | \
           docker run -i --rm --link clickhouse-server:clickhouse-client yandex/clickhouse-client -m --host clickhouse-server \
           --query="INSERT INTO ModulesData.{0} FORMAT CSV"'.format(self.table, self.path), shell=True) #запись в бд
