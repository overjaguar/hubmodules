from PySide2.QtCore import QObject, QThread, Signal
from workers.Worker import Worker
from pathlib import Path
import time
from subprocess import Popen, PIPE

class MiBandWorker(Worker): #класс с родителем воркер

    def run(self): #старт
        while not self.userWannaStop: #пока пользователь не захочет остановить
            self.process = Popen(['python3', self.pathToModule, self.mode], stdin=PIPE, stdout=PIPE, stderr=PIPE) #создание сабпроцесса
            self.path = ''

            self.isWorking = True #флаг работы
            while True: #бесконечный цикл
                line = self.process.stdout.readline()#прочитать строку
                
                if not line: #читаем пока что-то есть, а если нету ничего то выходим из цикла
                    break
                else:
                    line = str(line.rstrip(), 'utf-8') #читаем строку в кодировке ютф8 и убираем ненужные пробелы вконце

                if line.startswith("Results are saved in"):#если встречаем строку, которая начинается с этого
                    self.path = line[len("Results are saved in file: "):] #то возвращаем путь и названием модуля

                    if not self.userWannaStop: #если пользователь не хочет останавливать (только если пришел путь)
                        self.working.emit() #то отсылаем сигналтого, что работает
                        
                if line == "Connected" and not self.userWannaStop and self.mode == '-a': #если встретил строку коннектед
                    self.connected.emit() #имит состояния подключен

                self.message.emit(line)#имитим полученную строку
            
            self.isWorking = False #не работает

            if self.mode == '-t': #если режим работы калибровки то брейкаем
                break

            if self.process.poll() and not self.userWannaStop: #если произошла ошибки
                self.failed.emit() #подача сигнала фейла

            if not self.userWannaStop: # если преждевременно завершился, то происходит запись в бд
                self.dbSaving()