from PySide2.QtCore import QObject, QThread, Signal
from workers.Worker import Worker
from pathlib import Path
import time, os
from subprocess import Popen, PIPE

class TobiiGazeWorker(Worker): #класс с родителем воркер

    def run(self): #старт
        while not self.userWannaStop: #читаем поток вывода
            self.path = ''

            self.process = Popen([self.pathToModule, self.mode], stdin=PIPE, stdout=PIPE, stderr=PIPE) #создание сабпроцесса
            
            self.isWorking = True #теперь работает тру

            while True: #читаем поток вывода
                line = self.process.stdout.readline()#прочитать строку
                
                if not line: #читаем пока что-то есть, а если нету ничего то выходим из цикла
                    break
                else:
                    line = str(line.rstrip(), 'utf-8') #читаем строку в кодировке ютф8 и убираем ненужные пробелы

                if self.mode =="-a" and not self.userWannaStop: #если режим записи и пользователь не хочет останавливать
                    if line.startswith("Connected."): #если строка начинается с коннектед
                        self.connected.emit() #то посылаем сигнал о том, что подключен

                if line.startswith("Results are saved in"): #если начинается с этого <-
                    self.pathToFile = line[len("Results are saved in file: "):] #получение названием файла
                    pathToLog = Path.cwd() / self.pathToFile #добавление его к другому пути
                    pathToLog = pathToLog.resolve()
                    self.path = str(pathToLog) #передача пути и названия модуля

                    if not self.userWannaStop: #если пользовать не хочет останавливать
                        self.working.emit() #имит состояния работы

                self.message.emit(line)#имитим полученную строку

            self.isWorking = False #флаг о том, что воркер не работает
            
            if self.mode == '-t': #если режим калибровки
                break #выходил из цикла

            if self.process.poll() and not self.userWannaStop: #исли завершился ошибкой
                self.failed.emit() #имит ошибки
            
            if not self.userWannaStop: # если преждевременно завершился, то происходит запись в бд
                self.dbSaving()
