from PySide2.QtCore import QObject, QThread, Signal
from workers.Worker import Worker
from pathlib import Path
import time
from subprocess import Popen, PIPE
import subprocess

class EmotivEEGWorker(Worker): #класс с родителем воркер

    def run(self): #старт
        while not self.userWannaStop: #читаем поток вывода
            self.path = {} #словарь путей сохранения
            self.process = Popen([self.pathToModule, self.mode], stdin=PIPE, stdout=PIPE, stderr=PIPE) #создание сабпроцесса
            
            self.isWorking = True #теперь работает тру

            while True: #читаем поток вывода
                line = self.process.stdout.readline() #прочитать строку
                
                if not line: #читаем пока что-то есть, а если нету ничего то выходим из цикла
                    break
                else:
                    line = str(line.rstrip(), 'utf-8') #читаем строку в кодировке ютф8 и убираем ненужные пробелы

                if line.startswith("Results of EEG"): #если начинается с этого
                    self.path['eeg'] = line[len("Results of EEG are saved in: "):] #сохранение пути для ЭЭГ
                    if not self.userWannaStop: #если пользовательне хочет останавливать
                        self.working.emit() #имит того, что модуль работает

                if line.startswith("Results of Facial"):
                    self.path['facial'] = line[len("Results of Facial are saved in: "):] #сохранение пути для движений лица

                self.message.emit(line)#имитим полученную строку

            self.isWorking = False #установка того, что модуль не работает

            if self.mode == '-t': #если в режиме калибровки
                break #выход из цикла

            if self.process.poll(): #если ошибка
                self.failed.emit() #подача сигнала ошибки

            if not self.userWannaStop: # если преждевременно завершился, то происходит запись в бд
                self.dbSaving()

    def dbSaving(self):
        if len(self.path.keys()) != 2: #тип если не пришли все данные то выход
            return
                
        subprocess.run('cat {0} | pv | \
           docker run -i --rm --link clickhouse-server:clickhouse-client yandex/clickhouse-client -m --host clickhouse-server \
           --query="INSERT INTO ModulesData.EmotivEEG FORMAT CSV"'.format(self.path['eeg']), shell=True) #запись в бд
        
        subprocess.run('cat {0} | pv | \
           docker run -i --rm --link clickhouse-server:clickhouse-client yandex/clickhouse-client -m --host clickhouse-server \
           --query="INSERT INTO ModulesData.EmotivFacial FORMAT CSV"'.format(self.path['facial']), shell=True) #запись в бд