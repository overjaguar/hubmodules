#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

typedef struct test_info
{
	short gender; // 0 - male, 1 - female
	short age;
	int exp_duration;
	char graph_env[64];
	time_t start_time;
	time_t finish_time;
} TestInfo;

typedef struct pair
{
	unsigned char set;
	int frequency;
} Pair;

typedef struct cycle
{
	Pair *p;
	struct cycle *next;
} Cycle;

typedef struct timestamps
{
	time_t etalon_clicked_stamp;
	//time_t table_get_focus_stamp;
	time_t table_clicked_stamp;
	gboolean result;
} TimeStamps;


GtkWidget * create_etalon_window(void);
GtkWidget * create_table_window(void);

GtkBuilder * get_gtk_builder(const gchar *);

void show_warning_message(GtkWidget *, const gchar *);	// отображение предупреждающего сообщения
void table_displaying(gboolean);			// управление видимостью таблицы картинок
void etalon_displaying(gboolean);			// управление видимость эталона
void prepare_testing_process();			
void shuffle_the_set(unsigned char *);			// перемешивание элементов набора
char * get_next_image_from_set(unsigned char);
char * get_image_name_by_number(unsigned char, unsigned char);
void fill_the_table();
char * get_time_string(time_t);
void shutdown();

void write_experiment_header(TestInfo *);
void write_timestamps_into_logfile(unsigned char prev_cur_set, TimeStamps *);
void write_experiment_footer(time_t);

extern GtkWidget *main_window;
extern GtkWidget *male_radio;
extern GtkWidget *age_entry, *duration_entry, *env_entry;

extern GtkWidget *etalon_window;
extern GtkWidget *etalon_image;
extern GtkWidget *etalon_button;

extern GtkWidget *table_window;
extern GtkWidget *buttons_list[5][5];
extern GtkWidget *images_list[5][5];

extern FILE *logfile;

//extern gboolean permission;

extern short etalon_number, choose_number;
extern time_t rawtime;
extern char str_time_buffer[255];
extern TestInfo exp_info;
extern TimeStamps time_stamps;

extern Cycle *cycle_head, *cycle_cur;
extern int num_of_rep_lim, cur_num_of_rep;
extern unsigned char stage[25];
extern unsigned char cur_set;
