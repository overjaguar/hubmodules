#include "../include/header.h"

GtkWidget *etalon_image;
GtkWidget *etalon_button;

//gboolean permission;

char * get_next_image_from_set(unsigned char);
/*
 * Функция создания окна с эталоном. Интерфейс формируется
 * из файла etalon_window.ui
 */
GtkWidget * create_etalon_window(void)
{
	GtkWidget *window;
	GtkBuilder *builder;

	builder = get_gtk_builder("./interface/etalon_window.ui");

	window = GTK_WIDGET(gtk_builder_get_object(builder, "etalon_window"));
	etalon_image = GTK_WIDGET(gtk_builder_get_object(builder, "etalon_image"));
	etalon_button = GTK_WIDGET(gtk_builder_get_object(builder, "image_button"));

	gtk_builder_connect_signals(builder, NULL);
	g_object_unref(G_OBJECT(builder));

	gtk_image_set_from_file(GTK_IMAGE(etalon_image), get_next_image_from_set(cur_set));
	cur_num_of_rep++;

	return window;
}

/*
 * Функция получает очередной случайный эталон из набора
 */
char * get_next_image_from_set(unsigned char set_num)
{
	unsigned char img_num;
	img_num = random()%25 + 1;
	etalon_number = img_num;

	return get_image_name_by_number(set_num, img_num);
}

/*
 * Функция управляет отображением эталонной картинки. Если
 * visible = TRUE, то картинка отображается.
 */
void etalon_displaying(gboolean visible)
{
	if (visible)
	{
		gtk_widget_show(GTK_WIDGET(etalon_button));
	}
	else
	{
		gtk_widget_hide(GTK_WIDGET(etalon_button));
	}
}

/*
 * Функция перемешивает содержимое массива the_set
 */
void shuffle_the_set(unsigned char *set)
{
	int i = 0;
	unsigned char change;
	int rand_ind;

	for (i = 1; i < 25; i++)
	{
		rand_ind = random()%i;
		
		change = set[i];
		set[i] = set[rand_ind];
		set[rand_ind] = change;
	}
}
