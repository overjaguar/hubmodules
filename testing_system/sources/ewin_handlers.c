#include "../include/header.h"

/*
 * Функция подготавливает приложение к завершению работы
 */
/*void shutdown()
{
	write_experiment_footer(exp_info.finish_time);
	fclose(logfile);
	gtk_main_quit();
}*/

/*
 * Функция-обработчик сигнала закрытия окна с эталоном
 */
void on_etalon_window_destroy(GtkWidget *widget, gpointer data)
{
	time(&exp_info.finish_time);
	//shutdown();
	write_experiment_footer(exp_info.finish_time);
	fclose(logfile);
	gtk_main_quit();
}

/*
 * Функция-обработчик сигнала, который возбуждается
 * при щелчке по эталону
 */
void on_etalon_image_clicked(GtkWidget *widget, gpointer data)
{
	// Получаем временной штамп №1 - щелчок по эталонной картинке
	time(&time_stamps.etalon_clicked_stamp);

	fill_the_table();
	table_displaying(TRUE);
	etalon_displaying(FALSE);
	//permission = TRUE;
}
