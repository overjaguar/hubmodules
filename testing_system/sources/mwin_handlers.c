#include "../include/header.h"

GtkWidget *etalon_window;
GtkWidget *table_window;

TestInfo exp_info;

/*
 * Функция-обработчик события, которое возникает при закрытии главного окна
 */
gboolean on_main_window_delete_event(GtkWidget *window, GdkEvent *event, gpointer data)
{
	gboolean answer = TRUE;
// *** Закомментируйте этот блок, если не хотите, чтобы
// перед закрытием программа задавала вопрос
	
	GtkWidget *message_dialog;
	gint dialog_result;

	message_dialog = gtk_message_dialog_new(GTK_WINDOW(window), GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, "Вы уверены, что хотите прервать тестирование?");
	dialog_result = gtk_dialog_run(GTK_DIALOG(message_dialog));

	switch (dialog_result)
	{
	case GTK_RESPONSE_YES:
		answer = TRUE;
		break;
	case GTK_RESPONSE_NO:
		answer = FALSE;
		break;
	}
	gtk_widget_destroy(message_dialog);

// *********************	
	if (answer)
		return FALSE;
	return TRUE;
}

/*
 * Функция-обработчик сигнала, который возбуждается при
 * уничтожении главного окна.
 */
void on_main_window_destroy(GtkWidget *window, gpointer data)
{
	gtk_main_quit();
}

/*
 * Функция-обработчик сигнала, который возбуждается при
 * нажатии кнопки "Начать эксперимент"
 */
void on_start_button_clicked(GtkWidget *button, gpointer data)
{
	const gchar *age_entry_text, *duration_entry_text, *env_entry_text;
		
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(male_radio)))
	{
		exp_info.gender = 0;
	}
	else
	{
		exp_info.gender = 1;
	}

	age_entry_text = gtk_entry_get_text(GTK_ENTRY(age_entry));
	if ((strcmp(age_entry_text, "") == 0))
	{
		show_warning_message(main_window, "Укажите возраст тестируемого!");
		return;
	}
	exp_info.age = atoi(age_entry_text);

	duration_entry_text = gtk_entry_get_text(GTK_ENTRY(duration_entry));
	if (strcmp(duration_entry_text, "") == 0)
	{
		show_warning_message(main_window, "Укажите продолжительность эксперимента!");
		return;
	}
	exp_info.exp_duration = atoi(duration_entry_text);

	env_entry_text = gtk_entry_get_text(GTK_ENTRY(env_entry));
	if (strcmp(env_entry_text, "") == 0)
	{
		show_warning_message(main_window, "Введите название графической оболочки!");
		return;
	}
	strcpy(exp_info.graph_env, env_entry_text);

	prepare_testing_process();
	// Получаем время старта эксперимента
	time(&rawtime);
	exp_info.start_time = rawtime;
	// Заполняем заголовок в лог-файле полученной информацией
	write_experiment_header(&exp_info);

	gtk_widget_hide(main_window);

	etalon_window = create_etalon_window();
	table_window = create_table_window();
	
	gtk_widget_show(table_window);
	gtk_widget_show(etalon_window);
}

