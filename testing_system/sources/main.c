#include "../include/header.h"
#include <string.h>
#include <stdlib.h>
#include <time.h> 
//#include <gtk/gtk.h>
GtkWidget *main_window;
GtkWidget *male_radio;
GtkWidget *age_entry, *duration_entry, *env_entry;
GtkWidget *start_button;

short etalon_number = 0, choose_number = 0;

time_t rawtime;
TimeStamps time_stamps;
char str_time_buffer[255];
FILE *logfile;

Cycle *cycle_head = NULL, *cycle_cur = NULL;
int num_of_rep_lim = 0, cur_num_of_rep = 0;
unsigned char stage[25] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
			11, 12, 13, 14, 15, 16, 17, 18, 19,
			20, 21, 22, 23, 24, 25};
unsigned char cur_set = 0;

int get_test_configuration(FILE *);
Pair * parse_line(char *);

/*
 * Функция подгружает файл интерфейса в GtkBuilder
 */
GtkBuilder* get_gtk_builder(const char *uifile)
{
	GtkBuilder *builder;
	GError *error = NULL;

	builder = gtk_builder_new();
	if (!gtk_builder_add_from_file(builder, uifile, NULL))
	{
		g_critical("Невозможно загрузить файл интерфейса: %s\n", error->message);
		g_error_free(error);
		gtk_main_quit();
		return NULL;
	}

	return builder;
}


/*
 * Точка входа приложения. Производится инициализация gtk.
 * Формируется начальное окно приложения. Запускается главный цикл.
 */
int main(int argc, char *argv[])
{
	srand(time(NULL));
	GtkBuilder *builder;
	printf("OMG2\n");
	gtk_init(&argc, &argv);
	printf("OMG3\n");	
	builder = get_gtk_builder("./interface/start_window.ui");

	main_window = GTK_WIDGET(gtk_builder_get_object(builder, "main_window"));
	male_radio = GTK_WIDGET(gtk_builder_get_object(builder, "male_radiobutton"));
	age_entry = GTK_WIDGET(gtk_builder_get_object(builder, "age_entry"));
	duration_entry = GTK_WIDGET(gtk_builder_get_object(builder, "duration_entry"));
	env_entry = GTK_WIDGET(gtk_builder_get_object(builder, "env_entry"));
	start_button = GTK_WIDGET(gtk_builder_get_object(builder, "start_button"));

	gtk_builder_connect_signals(builder, NULL);
	g_object_unref(G_OBJECT(builder));

	gtk_widget_show(main_window);
	printf("OMG\n");
	gtk_main();

	return 0;
}

/*
 * Функция отображает всплывающее окно с предупреждающим
 * сообщением message и кнопкой ОК.
 */
void show_warning_message(GtkWidget *parent, const gchar *message)
{
	GtkWidget *message_dialog;

	message_dialog = gtk_message_dialog_new(GTK_WINDOW(parent), GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, GTK_BUTTONS_OK, "%s", message);
	gtk_dialog_run(GTK_DIALOG(message_dialog));
	gtk_widget_destroy(message_dialog);
}

void prepare_testing_process()
{
	const char *CONFIG_FILE_PATH = "./test-config";
	char nowTime[40];
	const time_t timer = time(NULL);
	struct tm *u;
	u = localtime(&timer);
	strftime(nowTime, 40, "%d_%m_%Y_%H_%M_%S", u);
	char* tmp = (char*)malloc(sizeof(nowTime) * 2);
	char *LOGFILE_PATH = "./logs/";
	strcpy(tmp, LOGFILE_PATH);
	strcat(tmp, nowTime);
	strcat(tmp, ".ts.csv");
	FILE *config_file;

	
	// Невозможно открыть файл конфигурации
	if ( (config_file = fopen(CONFIG_FILE_PATH, "r")) == NULL)
	{
		show_warning_message(main_window, "Не удается открыть конфигурационный файл эксперимента test-config.  Приложение завершит свою работу.");
		gtk_main_quit();
		return;
	}
	
	// TODO Что-то не так с конфигурацией...
	if (get_test_configuration(config_file) != 0)
	{
		show_warning_message(main_window, "Не удается прочитать конфигурацию эксперимента. Приложение завершит свою работу.");
		gtk_main_quit();
		return;
	}

	// Если файл конфигурации пуст
	if (cycle_head == NULL)
	{
		show_warning_message(main_window, "Конфигурация эксперимента не задана. Проверьте содержимое файла test-config. Приложение завершит свою работу.");
		gtk_main_quit();
		return;
	}

	// Все нормально. Конфигурация считана. Закрываем файл.
	fclose(config_file);
	cycle_cur = cycle_head;
	cur_set = cycle_cur->p->set;
	num_of_rep_lim = cycle_cur->p->frequency;

	if ( (logfile = fopen(tmp, "a")) == NULL)
	{
		show_warning_message(main_window, "Не удается открыть или создать файл журнала событий. Приложение завершит свою работу.");
		gtk_main_quit();
		return;
	}
	free(tmp);
}

/*
 * Функция выделяет из строки два значения: номер набора
 * и количество повторений
 */
Pair * parse_line(char *line)
{
	Pair *p;
	char *value;

	if (line != NULL)
	{
		// TODO: проверка на корректность содержимого конфигурационного файла 
		p = (Pair *) malloc(sizeof(Pair));
		value = strtok(line, " :");
		p->set = (unsigned char) atoi(value);
		value = strtok(NULL, " :");
		p->frequency = atoi(value);
	}
	else return NULL;
	
	return p;	
}

/*
 * Функция получает конфигурацию всего эксперимента
 * из файла test-config. Формирует список из элементов,
 * каждый из которых содержит номер серии и количество
 * раз, которое ее требуется отобразить.
 */
int get_test_configuration(FILE *cf)
{
	char str[255];
	Pair *p;
	Cycle *cyc;

	while (!feof(cf))
	{
		if (fgets(str, 254, cf))
		{
			if (strcmp(str, "\n") != 0)
			{
				if ( (p = parse_line(str)) == NULL)
					return 1;
			
				cyc = (Cycle *) malloc(sizeof(Cycle));
				cyc->p = p;
				cyc->next = NULL;
				if (cycle_head == NULL) cycle_head = cyc;
				else cycle_cur->next = cyc;
				cycle_cur = cyc;
			}
		}
	}

	return 0;
}

char * get_time_string(time_t time)
{
	strftime(str_time_buffer, 80, "%X", localtime(&time));
	
	return str_time_buffer;
}

void write_experiment_header(TestInfo *ti)
{
	strftime(str_time_buffer, 80, "%d %B %Y, %X", localtime(&(ti->start_time)));
	fprintf(logfile, "\"Эксперимент начат:\";\"%s\"\n", str_time_buffer);

	fprintf(logfile, "Пол;");
	if (!ti->gender) fprintf(logfile, "мужской\n");
	else fprintf(logfile, "женский\n");

	fprintf(logfile, "Возраст;%d\n", ti->age);
	fprintf(logfile, "\"Продолжительность эксперимента\";%d\n", ti->exp_duration);
	fprintf(logfile, "\"Графическая оболочка\";%s\n\n", ti->graph_env);
	
	fprintf(logfile, "Последовательность;Штамп1;Штамп2;Результат\n");
}

void write_timestamps_into_logfile(unsigned char prev_cur_set, TimeStamps *ts)
{
	fprintf(logfile, "%d;", prev_cur_set);
	fprintf(logfile, "%s;", get_time_string(ts->etalon_clicked_stamp));
	//fprintf(logfile, "%s;", get_time_string(ts->table_get_focus_stamp));
	fprintf(logfile, "%s;", get_time_string(ts->table_clicked_stamp));

	if (ts->result) fprintf(logfile, "Успешно\n");
	else fprintf(logfile, "Ошибка!\n");
}

void write_experiment_footer(time_t finish_time)
{
	fprintf(logfile, "\n\"Эксперимент закончен:\";\"%s\"\n\n\n\n", get_time_string(finish_time));
}
