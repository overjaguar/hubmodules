#include "../include/header.h"

/*
 * Функция-обработчик сигнала закрытия окна с таблицей
 */
void on_table_window_destroy(GtkWidget *widget, gpointer data)
{
	time(&exp_info.finish_time);
	shutdown();
}

/*
 * Функция-обработчик сигнала, который возникает при
 * нажатии на одну из картинок в таблице
 */
void on_table_image_clicked(GtkWidget *widget, gpointer image)
{
	int i = 0, j = 0;
	int x = 0, y = 0;
	
	unsigned char prev_cur_set = cur_set;

	// Получаем временной штамп №2 - щелчок по картинке из таблицы
	time(&time_stamps.table_clicked_stamp);

	for (i = 0; i < 5; i++)
		for (j = 0; j < 5; j++)
			if (widget == buttons_list[i][j]) 
			{
				x = j;
				y = i;
			}

	choose_number = stage[y*5+x];
	if (choose_number == etalon_number)
		time_stamps.result = TRUE;
	else
		time_stamps.result = FALSE;
	if(time_stamps.table_clicked_stamp - exp_info.start_time < exp_info.exp_duration * 60){
		if (cur_num_of_rep < num_of_rep_lim)
			cur_num_of_rep++;
		else
			if (cycle_cur->next != NULL)
			{
				cycle_cur = cycle_cur->next;
				cur_set = cycle_cur->p->set;
				num_of_rep_lim = cycle_cur->p->frequency;
				cur_num_of_rep = 1;
			}
			else
			{
				cycle_cur = cycle_head;
				cur_set = cycle_cur->p->set;
				num_of_rep_lim = cycle_cur->p->frequency;
				cur_num_of_rep = 1;
			}
	}
	else{
		time(&exp_info.finish_time);
		show_warning_message(table_window, "Время вышло. Программа завершит свою работу.");
		//shutdown();
		write_timestamps_into_logfile(prev_cur_set, &time_stamps);
		write_experiment_footer(exp_info.finish_time);
		fclose(logfile);
		gtk_main_quit();
		return;
	}
	gtk_image_set_from_file(GTK_IMAGE(etalon_image), get_next_image_from_set(cur_set));
	etalon_displaying(TRUE);
	table_displaying(FALSE);
	
	write_timestamps_into_logfile(prev_cur_set, &time_stamps);
	//permission = FALSE;
}
/*
gboolean on_table_window_state_event(GtkWidget *widget, GdkEvent *event, gpointer data)
{	
	if (gtk_window_is_active(GTK_WINDOW(widget)) && permission)
	{
		printf("is Active\n");
		permission = FALSE;
	}
	return TRUE;
}
*/
