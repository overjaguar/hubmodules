#include "../include/header.h"

GtkWidget *buttons_list[5][5];
GtkWidget *images_list[5][5];

const char *IMAGE_SETS_DIR = "./images/";

/*
 * Функция создания окна с таблицей. Интерфейс окна формируется
 * из файла table_window.ui
 */
GtkWidget* create_table_window(void)
{
	GtkWidget *window;
	GtkBuilder *builder;
	int i = 0, j = 0;
	gchar img_widget_name[6];
	gchar btn_widget_name[6];

	builder = get_gtk_builder("./interface/table_window.ui");

	window = GTK_WIDGET(gtk_builder_get_object(builder, "table_window"));

	for (i = 0; i < 5; i++)
		for (j = 0; j < 5; j++)
		{
			sprintf(btn_widget_name, "btn%d%d", i, j);
			buttons_list[i][j] = GTK_WIDGET(gtk_builder_get_object(builder, btn_widget_name));

			sprintf(img_widget_name, "img%d%d", i, j);
			images_list[i][j] = GTK_WIDGET(gtk_builder_get_object(builder, img_widget_name));
		}

	gtk_builder_connect_signals(builder, NULL);
	g_object_unref(G_OBJECT(builder));

	table_displaying(FALSE);
	//permission = FALSE;
	return window;
}

/*
 * Функция управляет отображение таблицы с картинками.
 * Если значение visible = TRUE, то таблица отображается.
 * В противном случае, таблица скрывается.
*/
void table_displaying(gboolean visible)
{
	int i, j;

	for (i = 0; i<5; i++)
		for (j = 0; j<5; j++)
		{
			if (visible)
			{
				gtk_widget_show(GTK_WIDGET(buttons_list[i][j]));
			}
			else
			{
				gtk_widget_hide(GTK_WIDGET(buttons_list[i][j]));
			}
		}
}

/*
 * Функция возвращает текстовую строку - путь к файлу изображения,
 * который формируется на основании номера серии и порядкового номера
 * изображения в серии.
 */

char * get_image_name_by_number(unsigned char num_of_set, unsigned char num_of_img)
{
	static char image_name[22];	

	sprintf(image_name, "%s%d/%d_%d.png", IMAGE_SETS_DIR, num_of_set, num_of_set, num_of_img);

	return image_name;
}


/*
 * Функция заполняет таблицу картинками.
 */
void fill_the_table()
{
	int i, j;
	int k;
	
	k = 0;
	for (i = 0; i < 5; i++)
		for (j = 0; j < 5; j++)
		{
			gtk_image_set_from_file(GTK_IMAGE(images_list[i][j]), get_image_name_by_number(cur_set, stage[k]));
			k++;
		}
}
