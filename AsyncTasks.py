import config
import time
from PySide2.QtCore import QObject, QThread, Signal
from subprocess import Popen, PIPE

from TextEdit import TextEdit

workersClasses, plotsClasses, pathsToModules, tables = config.resolveClasses() # заполнение словарей из конфига

class AsyncTasks(QObject): #класс в котором будет вестись асинхронная работа
    freeze = Signal() #объект для блокировки нажатия кнопки старт
    finished = Signal() #а этот объект нам нужен для блокировки кнопки стоп
    stopping = Signal() #сигнал остановки
    
    def __init__(self, parent):#конструктор
        super().__init__(parent) #наследование методов родителям
        self.threadDict = dict() #создание словаря потоков
        self.workers = dict() #создание слова воркеров
        self.plots = dict() #словарь графиков
        self.terminals = dict() #словарь терминалов режима калибровки

    def runModules(self, modules, mode): #запуск модулей, сюда передается словарь с какими модулями выполнять и режим работы
        self.freeze.emit() #имитим блокирование кнопочки
        self.workers.clear() # очистка старых данных
        self.plots.clear()
        self.terminals.clear()
        self.threadDict.clear()

        for module in modules: #для всех модулей
            if not modules[module]: #если модуль фалсе, то прощай (то есть он не выбран)
                continue

            thread, worker = self.initThread(module, mode) # инициализация потока и воркера (смотреть внизу)

            if not (thread == None or worker == None): # если все определено
                self.threadDict[module], self.workers[module] = thread, worker #добавляем в словари объекты потока и воркера
            else: # если что-то не определено
                print('ERROR: For module with name \"{}\" worker is not defined. CONTINUE'.format(module)) # выпод ошибки
                continue # переход к следующему

            if(mode == '-a'): #если режим работы с записью
                if module not in plotsClasses: # если не определен график класса
                    print('ERROR: For module with name \"{}\" plot is not defined, module is working at background. CONTINUE'.format(module)) # вывод ошибки
                    continue # переход к следующему

                self.plots[module] = plotsClasses[module](name=module + " plot") #создание виджета с графиком для каждого свой в соотвествии с конфигом
                self.workers[module].message.connect(self.plots[module].update_plot) #обновление графика при получении сообщения
                self.plots[module].show() #вывод графика
            else: #если калибровка
                self.terminals[module] = TextEdit(module) #создаем окно вывода стандартного потока вывода
                self.workers[module].message.connect(self.terminals[module].insertText)#вставить текст туда
                self.terminals[module].show()#вывод
            
        for thread in self.threadDict:   #запуск потоков
            self.threadDict[thread].start()

    def stopAllTasks(self): #остановка всех модулей
        self.stopping.emit() #имитим остановку

        for worker in self.workers: #воркер принимает значение названия воркеров
            self.workers[worker].stop() #вызов метода остановки воркеров

        for plot in self.plots: #для всех графиков:
            self.plots[plot].close() #закрыть график

        for terminal in self.terminals: #для всех терминалов:
            self.terminals[terminal].close() #закрыть терминал

        self.finished.emit() #имитим конец 

    def getWorker(self, nameOfWorker): #получить воркер
        return self.workers[nameOfWorker] if nameOfWorker in self.workers else None

    def initThread(self, nameOfModule: str, mode: str):
        if not nameOfModule in workersClasses:
            return [None, None] # если нету такого в воркера, то ничего не возвращает

        thread = QThread() #создание потока
        worker = workersClasses[nameOfModule](nameOfModule, mode, pathsToModules[nameOfModule], tables[nameOfModule]) # создание экземпляра класса и передача ему имени, режима работы, пути до модуля и названия таблицы, в которую сохранять
        worker.moveToThread(thread) #добавляем воркер в поток
        thread.started.connect(worker.run) #запуск воркера
        worker.finished.connect(thread.quit) #завершение работы потока
        worker.finished.connect(worker.deleteLater) #удалем при окончании объект
        thread.finished.connect(thread.deleteLater) #удаляем поток
        return [thread, worker] #возращаем объект потока и воркера
