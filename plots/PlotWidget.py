#!/usr/bin/env python3
import sys

from PySide2.QtCore import QFile, QObject, Signal, Slot, QTimer
from PySide2.QtWidgets import QApplication, QVBoxLayout, QWidget
from PySide2.QtUiTools import QUiLoader

from pytz import utc
from datetime import datetime
from time import mktime

import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

from abc import abstractmethod

import random
#импорт 1001 либы)

class PlotWidget(QWidget):
    def __init__(self, parent=None, name="Plot"): #конструктор
        super().__init__(parent) #наследование

        fig = Figure(figsize=(7, 5), dpi=65, facecolor=(1, 1, 1), edgecolor=(0, 0, 0)) #контейнер верхнего уровня графиков. Тип та область в которой рисуется
        self.canvas = FigureCanvas(fig) #создание холста
        self.toolbar = NavigationToolbar(self.canvas, self) #создание навигации
        lay = QVBoxLayout(self) #что-то типа вертикальной сетки
        lay.addWidget(self.toolbar) #добавление в нее тулбара
        lay.addWidget(self.canvas) #добавление на нее графика
        self.ax = fig.add_subplot(111) #создание сетки
        self.setWindowTitle(name) #название окна
        self.lines = {} #словарь линий графика
        self.values = {} #словарь значений

    #For case when data is formatted as "Data \NumOfParams\ Time: \data\ \time\ Param1: \value\ Param2: \value\"
    def update_plot(self, newValue): # обновление графика
        if not newValue.startswith('Data'): #если не начинается со слова Data
            return #то ниче не делаем

        newValue = newValue.split() # разбивание новой строки на лексемы

        NumOfParams = int(newValue[1]) # первый аргумент это количество параметров (не более 9, включая время)

        if NumOfParams < 2: # если параметров меньше двух то возврат
            return

        time = datetime.strptime(newValue[3] + ' ' + newValue[4], '%Y-%m-%d %H:%M:%S') #получение времени и перевод в UTC
        
        if 'Time' not in self.values: # если первое значение
            self.startTime = time # то становится начальным временем
            self.values['Time'] = [0]
        else:
            self.values['Time'].append(time - self.startTime) # иначе отнимает текущее время от начального

        params = [] # список параметров

        if len(self.lines.keys()) == 0: # если в первый раз пришла строка
            for paramNum in range(NumOfParams - 1): # для каждого параметра (-1 так как времяя исключается)
                nameOfParam = newValue[paramNum * 2 + 5] # задается имя параметра в соотвествии с лексемой
                nameOfParam = nameOfParam[:len(nameOfParam) - 1] # уменьшение длины так как есть еще двоеточие

                value = float(newValue[paramNum * 2 + 6]) # непосредственно значение

                self.lines[nameOfParam], *_ = self.ax.plot([]) # график линии параметра
                self.values[nameOfParam] = [value] # сохранение значения
            
            self.ax.legend( tuple(self.lines.values()), tuple(self.lines.keys()) ) #легенда (сами линии и названия их)
        else: # ну или если данные пришли не в первый раз  
            for paramNum in range(NumOfParams - 1): # аналогично для каждого парметра
                nameOfParam = newValue[paramNum * 2 + 5] # считывание значения
                nameOfParam = nameOfParam[:len(nameOfParam) - 1] # имя параметра
                value = float(newValue[paramNum * 2 + 6]) # само значение

                self.values[nameOfParam].append(value) # сохранение

        if len(self.values['Time']) >= 50 : #если длинна 50 значений со временем уже
            for param in self.values:
                self.values[param].pop(0) # выкидываются первые значения везде

        for param in self.lines: 
            self.lines[param].set_data(self.values['Time'], self.values[param]) # непосредственное добавление в график

        self.ax.set_xlim(self.values['Time'][0], max(self.values['Time'])) #длинна по х от 0 до максимального значения

        maxes = [] # список макс значений
        for setOfValues in self.values.values():
            maxes.append(max( tuple(self.values[setOfValues]) )) # поиск максимального из каждого набора

        self.ax.set_ylim( 0, max(maxes) ) #длинна по у от 0 до максимального из максимальных)

        self.canvas.draw() #непосрдественное рисование
