CREATE DATABASE IF NOT EXISTS ModulesData;

CREATE TABLE IF NOT EXISTS ModulesData.Miband (
	miband_date DateTime,
	miband_hrm Int16
) engine = MergeTree() ORDER BY miband_date;

CREATE TABLE IF NOT EXISTS ModulesData.EmotivEEG (
	emotiv_eeg_date DateTime,
	theta Float32,
	alpha Float32,
	low_beta Float32,
	high_beta Float32,
	gamma Float32,
	eeg_tag String
) engine = MergeTree() ORDER BY emotiv_eeg_date;

CREATE TABLE IF NOT EXISTS ModulesData.EmotivFacial (
	emotiv_facial_date DateTime,
	blink Float32,
	wink_left Float32,
	wink_right Float32,
	surprise Float32,
	frown Float32,
	smile Float32,
	clench Float32
) engine = MergeTree() ORDER BY emotiv_facial_date;

CREATE TABLE IF NOT EXISTS ModulesData.Tobii (
	tobii_date DateTime64,
	left_x Float32,
	left_y Float32,
	right_x Float32,
	right_y Float32
) engine = MergeTree() ORDER BY tobii_date;

SHOW TABLES FROM ModulesData;

SELECT * FROM ModulesData.Miband;

SELECT * FROM ModulesData.EmotivEEG;

SELECT * FROM ModulesData.EmotivFacial;

SELECT * FROM ModulesData.Tobii;

SHOW DATABASES;
