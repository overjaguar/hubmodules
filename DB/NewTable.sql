CREATE DATABASE IF NOT EXISTS ModulesData;

CREATE TABLE IF NOT EXISTS ModulesData.NewTable ( -- ЗДЕСЬ ЗАДАЕТСЯ НАЗВАНИЕ ТАБЛИЦЫ
	new_time DateTime, -- НЕОБХОДИМО СОЗДАТЬ АТРИБУТ С ДАТОЙ И ВРЕМЕНЕМ
	new_value Float64 -- ЕСЛИ НЕ ЗНАТЬ КАКОЙ ЛУЧШЕ ИСПОЛЬЗОВАТЬ, ТО ЛУЧШЕ ПО УМОЛЧАНИЮ Float64
	-- attribute Type < ПРИМЕР СТРОКИ. ПРИ ДОБАВЛЕНИИ, В КОНЦЕ ПРОШЛОЙ НЕОБХОДИМО ПОСТАВИТЬ ЗАПЯТУЮ!
) engine = MergeTree() ORDER BY miband_date; -- ВЫБОР ДВИЖКА ( ПО УМОЛЧАНИЮ МОЖНО ПОЛЬЗОВАТЬСЯ MergeTree), плюс тут первичный ключ задается. Но тут имеет смысл только такой.


