import os
from pathlib import Path

from workers.MiBandWorker import MiBandWorker # ПОДКЛЮЧЕНИЕ ВОРКЕРОВ
from workers.TobiiGazeWorker import TobiiGazeWorker
from workers.EmotivEEGWorker import EmotivEEGWorker
from workers.Worker import Worker

from plots.MibandPlot import MibandPlot # ПОДКЛЮЧЕНИЕ ГРАФИКОВ
from plots.TobiiPlot import TobiiPlot
from plots.EmotivPlot import EmotivPlot
from plots.PlotWidget import PlotWidget

def resolveClasses():
    path = os.path.abspath(__file__)
    pathToFile = Path(path[:path.rfind('/')]) # путь до текущего файла

    workersClasses = {
        'MiBand' : MiBandWorker,
        'TobiiGaze' : TobiiGazeWorker,
        'EmotivEEG' : EmotivEEGWorker # ПРИ ДОБАВЛЕНИИ НЕ ТЕРЯТЬ ЗДЕСЬ ЗАПЯТУЮ
        # 'NewModule' : Worker <ЗДЕСЬ ПИСАТЬ СВОЙ ВОРКЕР ПРИ ЖЕЛАНИИ ДОБАВИТЬ МОДУЛЬ. КЛАСС Worker МОЖЕТ ИСПОЛЬЗОВАТЬСЯ ПО УМОЛЧАНИЮ
    } # классы воркеров, выполняюих роль работы с модулям биометрии.

    plotsClasses = {
        'MiBand' : MibandPlot,
        'TobiiGaze' : TobiiPlot,
        'EmotivEEG' : EmotivPlot # ПРИ ДОБАВЛЕНИИ НЕ ТЕРЯТЬ ЗДЕСЬ ЗАПЯТУЮ
        # 'NewModule' : PlotWidget <ЗДЕСЬ МОЖНО ЗАДАТЬ СВОЙ ГРАФИК. ПО УМОЛЧАНИЮ ПОДХОДИТ PlotWidget, ОДНАКО ДАННЫЕ ДОЛЖНЫ СООТВЕСТВОВАТЬ ТОМУ, ЧТО ОПИСАНО В РУКОВОДСТВЕ
    } # словарь графиков.

    pathsToModules = {
        'MiBand' : pathToFile / 'modules' / 'MiBand' / 'miband2.py',
        'EmotivEEG' : pathToFile / 'modules' / 'EmotivEEG' / 'EmotivEEG',
        'TobiiGaze' : pathToFile / 'modules' / 'TobiiGaze' / 'Samples' / 'tracker'  # ПРИ ДОБАВЛЕНИИ НЕ ТЕРЯТЬ ЗДЕСЬ ЗАПЯТУЮ
        # 'NewModule' : путь_до_модуля < ЗДЕСЬ ЗАДАЕТСЯ ПУТЬ ДО МОДУЛЯ БИОМЕТРИИ
    } # словарь путей до модулей биометрии

    tables = {
        'TobiiGaze' : 'Tobii',
        'EmotivEEG' : 'EmotivEEG',
        'MiBand' : 'Miband' # ПРИ ДОБАВЛЕНИИ НЕ ТЕРЯТЬ ЗДЕСЬ ЗАПЯТУЮ
        # 'NewModule' : 'NewModuleTable' <ЗДЕСЬ ЗАДАЕТСЯ ТАБЛИЦА, В КОТОРОЙ ХРАНЯТСЯ ДАННЫЕ В БД. ДЛЯ ДОБАВЛЕНИЯ В ПАПОЧКЕ BD ЕСТЬ ФАЙЛ NewTable.
    } # Словарь соответствия таблиц в БД

    return [workersClasses, plotsClasses, pathsToModules, tables] # ну и возвращаются все эти словари